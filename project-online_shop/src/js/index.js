'use strict';

import 'bootstrap';

import { validate } from './form-validate.js';
import { toggleBurgerClass } from "./burger-menu.js";
import { scrollMenu } from "./scrollMenu.js";

validate();

toggleBurgerClass();

scrollMenu();


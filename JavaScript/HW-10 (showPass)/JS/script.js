const showPass = document.getElementById('password');
showPass.addEventListener('click', () => {

    const password = document.querySelector('.password');
    const elem = document.querySelector('#password');

    if (password.type === 'text') {
        password.type = 'password';
        elem.classList.replace('fa-eye', 'fa-eye-slash');
    } else {
        password.type = 'text';
        elem.classList.replace('fa-eye-slash', 'fa-eye');
    }
});
const confirm = document.getElementById('confirm');
confirm.addEventListener('click', () => {
    const password = document.querySelector('.confirm');
    const elem = document.querySelector('#confirm');

    if (password.type === 'text') {
        password.type = 'password';
        elem.classList.replace('fa-eye', 'fa-eye-slash');
    } else {
        password.type = 'text';
        elem.classList.replace('fa-eye-slash', 'fa-eye');
    }
});
const button = document.getElementById('btn');
button.addEventListener('click', () => {
    if (document.querySelector('.password').value === document.querySelector('.confirm').value) {
        alert('You are welcome');
    } else {
        const text = document.getElementById('text');
        text.innerText = 'Нужно ввести одинаковые значения';
        text.style.color = 'red';
    }
})
let userNumber = Number(prompt('Enter your number'));
while (userNumber == '' || isNaN(userNumber) || userNumber === null) {
    userNumber = Number(prompt('Enter your number'));
}
if (userNumber < 0) {
    alert('Sorry, no numbers');
}
for (let i = 0; i <= userNumber; i++) {
    if (i % 5 === 0) {
        console.log(i);
    }
}
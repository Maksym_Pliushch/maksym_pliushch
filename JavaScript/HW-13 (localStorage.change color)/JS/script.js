const button = document.getElementById('button');
button.addEventListener('click', () => {
    if (localStorage.getItem('bgc') === 'red') {
        localStorage.setItem('bgc', 'white');
    } else {
        localStorage.setItem('bgc', 'red');
    }
    if( localStorage.getItem('bgc') === 'red') {
        document.querySelector('body').style.backgroundColor = 'red';
        document.querySelector('p').style.color = 'yellow';
    } else {
        document.querySelector('body').style.backgroundColor = 'white';
        document.querySelector('p').style.color = 'black';
    }
});
document.addEventListener('DOMContentLoaded', () => {
    if ((localStorage.getItem('bgc')) === 'red') {
        document.querySelector('body').style.backgroundColor = 'red';
        document.querySelector('p').style.color = 'yellow';
    } else {
        document.querySelector('body').style.backgroundColor = 'white';
        document.querySelector('p').style.color = 'black';
    }
});
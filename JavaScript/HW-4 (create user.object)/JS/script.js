function createNewUser() {
    let userName = prompt('Enter your name');
    while (userName === null || userName === '' || !isNaN(userName)) {
        userName = prompt('Enter your name, again');
    }
    let userSurname = prompt('Enter your surname');
    while (userSurname === null || userSurname === '' || !isNaN(userSurname)) {
        userSurname = prompt('Enter your surname, again');
    }
    let newUser = {
        firstName: userName,
        lastName: userSurname,
        getLogin() {
            let fullName = (this.firstName.charAt(0) + this.lastName).toLowerCase();
            return fullName;
        }
    };
    console.log(newUser.getLogin());
    return newUser;
}

console.log(createNewUser());
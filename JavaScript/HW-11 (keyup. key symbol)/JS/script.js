const keyUp = document.addEventListener('keyup', (event) => {
    const buttons = document.querySelectorAll('button');
    buttons.forEach((btn) => {
        btn.style.backgroundColor = '#33333a';
    });

    if (event.code === 'Enter') {
        document.getElementById('enter').style.backgroundColor = 'blue';
    } else if (event.code === 'KeyS') {
        document.getElementById('s').style.backgroundColor = 'blue';
    } else if (event.code === 'KeyE') {
        document.getElementById('e').style.backgroundColor = 'blue';
    } else if (event.code === 'KeyO') {
        document.getElementById('o').style.backgroundColor = 'blue';
    } else if (event.code === 'KeyN') {
        document.getElementById('n').style.backgroundColor = 'blue';
    } else if (event.code === 'KeyL') {
        document.getElementById('l').style.backgroundColor = 'blue';
    } else if (event.code === 'KeyZ') {
        document.getElementById('z').style.backgroundColor = 'blue';
    }
});
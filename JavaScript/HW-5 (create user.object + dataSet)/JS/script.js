function createNewUser() {
    let userName = prompt('Enter your name');
    while (userName === null || userName === '') {
        userName = prompt('Enter your name, again');
    }
    let userSurname = prompt('Enter your surname');
    while (userSurname === null || userSurname === '') {
        userSurname = prompt('Enter your surname, again');
    }
    let userBirthday = prompt('Enter your birthday','dd.mm.yyyy');
    let newUser = {
        firstName: userName,
        lastName: userSurname,
        birthday: userBirthday,
        getLogin() {
            let fullName = (this.firstName.charAt(0) + this.lastName).toLowerCase();
            return fullName;
        },
        getAge() {
            let parseDate = this.birthday.split('.');
            let userDate = new Date(`${parseDate[1]}.${parseDate[0]}.${parseDate[2]}`);
            let userYears = Math.floor((new Date() - +userDate) / 3.154e10);
            return userYears;
        },
        getPassword() {
            let userPassword = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
            return userPassword;
        }
    };
    console.log(newUser.getLogin());
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
    return newUser;
}
console.log(createNewUser());